package pakiet;

public enum Calc {

	PLUS("+"), MINUS("-"), RAZY("*"), PRZEZ("/");
	
	
	private String operation;
	
	Calc (String operation){
		this.operation = operation;
	}
	
	public String getOperation(){
		return operation;
	}
	
	public double dodaj(double a, double b) {
		return a+b;
	}
	
	public double odejmij(double a, double b) {
		return a-b;
	}
	
	public double pomnoz(double a, double b) {
		return a*b;
	}
	
	public double podziel(double a, double b) {
		return a/b;
	}
	
}
