package pakiet;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import static pakiet.Calc.*;
import pakiet.Calc;




public class App {

	public static void main(String[] args) {

		System.out.println("Podaj działanie, które chcesz wykonać. \n "
				+ "Dostępne operatory to: +, -, *, /");
		
		Scanner scanner = new Scanner(System.in);
		
		String dzialanie = scanner.nextLine();
		
		String tab[] = dzialanie.split("[\\+\\*\\-\\/]");
		
		double a;
		double b;
		try{
		
		a = Double.parseDouble(tab[0]);
		b = Double.parseDouble(tab[1]);
		
		
		switch(getOperator(dzialanie)){
		case PLUS:
			System.out.println(PLUS.dodaj(a,b));
			break;
		case MINUS:
			System.out.println(MINUS.odejmij(a,b));
			break;
		case RAZY:
			System.out.println(RAZY.pomnoz(a,b));
			break;
		case PRZEZ:
			if(b!=0){
			System.out.println(PRZEZ.podziel(a,b));}
			else {System.out.println("Nie można dzielić przez 0!!!");}
			break;
		default:
			System.out.println("Błędny operator");
			break;
		
		}	
		} catch (java.lang.NumberFormatException e){
			System.out.println("Nie wprowadzono liczb.");
		}
		
		scanner.close();
	}

	public static Calc getOperator(String dzialanie){
	if(dzialanie.indexOf(PLUS.getOperation())!=-1){
		int operator = dzialanie.indexOf(PLUS.getOperation());
		return PLUS;
	}else if(dzialanie.indexOf(MINUS.getOperation())!=-1){
		int operator = dzialanie.indexOf(MINUS.getOperation());
		return MINUS;
	}else if(dzialanie.indexOf(RAZY.getOperation())!=-1){
		int operator = dzialanie.indexOf(RAZY.getOperation());
		return RAZY;
	}else if(dzialanie.indexOf(PRZEZ.getOperation())!=-1){
		int operator = dzialanie.indexOf(PRZEZ.getOperation());
		return PRZEZ;
	}else {
		System.out.println("Niepoprawny operator.");
		return null;
	}
	}
}
