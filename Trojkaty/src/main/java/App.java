import java.util.Arrays;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {

		System.out.println("To jest program Trójkąty i kąty!");
		Scanner scanner = new Scanner(System.in);
		
		for(;;) {
		
		System.out.println("Jeśli chcesz wprowadzić długość boków wpisz: "
				+ "boki, jeśli chcesz wprowadzić kąty wpisz: katy. Jeśli chcesz zakończysz wpisz: end.");
		String action = scanner.nextLine();
		
		action = action.toLowerCase();
		
		switch(action) { 
		case "boki":
			System.out.println("Podaj długości boków rozdzielone średnikami.");
			String boki = scanner.nextLine();
			String[] bokitab = boki.split(";");
			System.out.println(Arrays.toString(bokitab));
			int a = Integer.parseInt(bokitab[0]);
			int b = Integer.parseInt(bokitab[1]);
			int c = Integer.parseInt(bokitab[2]);

			if(a<b+c && b<a+c && c<a+b){
				System.out.println("Z podanych boków można zbudować trójkąt.");
			} else {
				System.out.println("Z podanych boków nie można zbudować trójkąta.");
			}
			break;
		case "katy":
			System.out.println("Podaj wartosci kątów rozdzielone średnikami.");
			String katy = scanner.nextLine();
			String[] katytab = katy.split(";");
			System.out.println(Arrays.toString(katytab));
			int d = Integer.parseInt(katytab[0]);
			int e = Integer.parseInt(katytab[1]);
			int f = Integer.parseInt(katytab[2]);

			if(d+e+f == 180){
				System.out.println("Podane kąty są kątami trójkąta.");
			} else {
				System.out.println("Podane kąty nie są kątami trójkąta.");
			}
			break;
		case "end":
			System.out.println("Program został zakończony.");
			scanner.close();
			System.exit(0);
		default:
			System.out.println("Błędnie wprowadzona komenda.");
			break;
		}
	}
	}
	
}
