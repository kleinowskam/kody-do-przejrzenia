package package2;

import java.util.ArrayList;
import static package2.Pozycja.*;

public class Zawodnik {

	private final String name;
	private final String lastName;
	private String club;
	private ArrayList<String> historia;
	private final Pozycja pozycja;
	
	public Zawodnik(String name, String lastName, String club, ArrayList<String> historia, Pozycja pozycja) {
		this.name = name;
		this.lastName = lastName;
		this.club = club;
		this.historia = historia;
		this.pozycja = pozycja;
	}
	
	public Zawodnik(String name, String lastName, String club, Pozycja pozycja) {
		this(name,lastName,club,new ArrayList<String>(),pozycja);
	}
	
	public String getName() {
		return name;
	}
	
	public String getLastName() {
		return lastName;
	}
	public String getPlayerInfo() {
		return String.join(" ", name, lastName, pozycja.toString());
	}
	
	public String getPersonalDetails() {
		return String.join(" ", name, lastName);
	}
	
	public String getClub() {
		return club;
	}

	public ArrayList<String> getHistoria() {
		return historia;
	}

	public void setHistoria(ArrayList<String> historia) {
		this.historia = historia;
	}

	public void setClub(String club) {
		historia.add(this.club);
		this.club = club;
	}
	
	public String getInfoToFile(){
		return String.join(";", name, lastName, club, historia.toString(), pozycja.toString());
	}
}
