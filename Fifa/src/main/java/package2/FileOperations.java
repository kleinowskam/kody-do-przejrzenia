package package2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileOperations extends File {
        private static final long serialVersionUID = 1L;

        private File file;

        // Wchodzi plik, imie i nazwisko zawodnika
        // wychodzi plik
        public FileOperations(File plik, String imie, String nazwisko) throws IOException {
                super(plik.getAbsolutePath());
                String sciezkaDostepu = (plik.getAbsolutePath() + "\\" + imie + "_" + nazwisko + ".txt");
        this.file = new File(sciezkaDostepu);
        if (!plik.exists()){
                plik.createNewFile();
        }
        }

        // Dostaje pelny tekst
        // Dodaje tekst do pliku (nie podmienia)
        public void zapis(String tekst){
        try(BufferedWriter zapis = new BufferedWriter(new FileWriter(file))) {
            zapis.write(tekst);
        } catch (IOException e) {
                System.out.println("Zapis sie nie powiodl :(");
        }
        }

        // Funkcja zwraca tekst z pliku. Jeżeli nie znajdize pliku zwroci null
    public String load() {
        String tekst = null;
        try(BufferedReader wczytywanie = new BufferedReader(new FileReader(file))) {
            tekst = wczytywanie.readLine();
        } catch (IOException e) {
            System.out.println("Wczytywanie sie nie powiodlo");
        }
        return tekst;
    }

}