package package2;

public class FifaZawodnikException extends Exception {

	private static final long serialVersionUID = -1924227326858153817L;

	public FifaZawodnikException() {
		super("Zawodnik o podanym imieniu i nazwisku istnieje już w bazie. Wprowadź dane ponownie.");
	}
}
