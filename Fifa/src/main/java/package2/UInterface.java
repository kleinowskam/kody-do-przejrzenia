package package2;

import static package2.App.scanner;
import static package2.App.zawodnicy;

import java.util.Arrays;

public class UInterface implements EdycjaDanych {

	//public static Scanner scanner = new Scanner(System.in);
	
	public Pozycja sprawdzPozycje(String dane) {
		switch (dane.toUpperCase()) {
		case "BRAMKARZ":
			return Pozycja.BRAMKARZ;
		case "OBRONCA":
			return Pozycja.OBRONCA;
		case "OBROŃCA":
			return Pozycja.OBRONCA;
		case "NAPASTNIK":
			return Pozycja.NAPASTNIK;
		case "POMOCNIK":
			return Pozycja.POMOCNIK;
		default:
			System.out.println("Błędna pozycja");
			return null;
		}
	}
	@Override
	public Zawodnik dodaj() throws FifaZawodnikException {
		System.out.println("Dodajesz właśnie nowego zawodnika.");
		
		System.out.println("Podaj imię i nazwisko zawodnika.");
		
		String dane = scanner.nextLine();
		
		String daneTab[] = dane.split(" ");
		System.out.println(Arrays.toString(daneTab));
		
		String imie = daneTab[0];
		String nazwisko = daneTab[1];
		
		boolean isRepeat = false;
		
		for (Zawodnik zw2: zawodnicy){
			if(zw2.getPersonalDetails().equals(dane)){
				isRepeat = true;
			}
			
		}if(isRepeat){throw new FifaZawodnikException();}
		
		Pozycja pozycja = null;
		do {
		System.out.println("Podaj pozycję zawodnika.");
		
		dane = scanner.nextLine();
		
		pozycja = sprawdzPozycje(dane);}
		while(pozycja==null);
		
		System.out.println("Podaj klub zawodnika.");
		
		dane = scanner.nextLine();
		
		String club = dane;
		
		Zawodnik zawodnik = new Zawodnik(imie, nazwisko, club, pozycja);
		
		System.out.println("Dzięki za dodanie zawodnika do naszego klubu! <3");
		
		return zawodnik;
	}

	@Override
	public void edytuj(Zawodnik z) {
		System.out.println("Podaj nowy klub.");
		
		String dane = scanner.nextLine();
		
		z.setClub(dane);
		
		System.out.println("Zmieniono klub zawodnikowi.");
	}

}
