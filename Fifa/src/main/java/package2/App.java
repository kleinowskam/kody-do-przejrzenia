package package2;

import static package2.App.scanner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class App {

	public static Scanner scanner = new Scanner(System.in);
	public static ArrayList<Zawodnik> zawodnicy = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		/*ArrayList<String> kluby = new ArrayList<>();
		kluby.add("Legia");
		kluby.add("Lechia");
		Zawodnik lewy = new Zawodnik("Robert", "Lewy", "Bayern", kluby ,Pozycja.NAPASTNIK);
		System.out.println(lewy.getInfoToFile());*/
	//	ArrayList<Zawodnik> zawodnicy = new ArrayList<>();
		UInterface ui = new UInterface();
		
		File[] fileList;
        File folder = new File("C:\\Java");
        if(!folder.exists()){
                folder.mkdir();
        }
        
        ArrayList<String> temp = new ArrayList<>();
        temp.clear();
        fileList = folder.listFiles();
        for (File file : fileList) {

               BufferedReader wczytywanie = new BufferedReader(new FileReader(file));
               String tekst = wczytywanie.readLine();
               String[] zene = tekst.split(";");
               for (String z : zene) {
                      if (z.startsWith("[")) {
                             z = z.substring(1, z.length() - 1);
                             String[] y = z.split(";");
                            // System.out.println(Arrays.toString(y));
                             for (String x : y)
                                   temp.add(x);
                      }
               }
               Pozycja tempP = null;
               if (zene[4].equals("POMOCNIK"))
                      tempP = Pozycja.POMOCNIK;
               if (zene[4].equals("OBRONCA"))
                      tempP = Pozycja.OBRONCA;
               if (zene[4].equals("NAPASTNIK"))
                      tempP = Pozycja.NAPASTNIK;
               if (zene[4].equals("BRAMKARZ"))
                      tempP = Pozycja.BRAMKARZ;

               Zawodnik koles = new Zawodnik(zene[0], zene[1], zene[2], temp, tempP);
               zawodnicy.add(koles);
             //  System.out.println(koles.getPlayerInfo());

        }
		
		System.out.println("Witaj w kartotece zawodników.");
		for(;;){
		System.out.println("Dostępne opcje: Dodaj [1], Edytuj [2], Przegladaj [3], Zakończ [END]. \nWprowadź wartość:");
		
		String dane = scanner.nextLine().toUpperCase();
	
			switch(dane) {
			case "1":
				Zawodnik player=null;
				try {
					player = ui.dodaj();
				} catch (FifaZawodnikException e) {
					System.out.println(e);
					break;
				}
				zawodnicy.add(player);
				FileOperations file = new FileOperations(folder,player.getName(),player.getLastName());
		        file.zapis(player.getInfoToFile());
				break;
			case "2":
				for(Zawodnik z1: zawodnicy) {
					System.out.println((zawodnicy.indexOf(z1) + 1)+ " " + z1.getPlayerInfo());
				}
				System.out.println("Podaj numer zawodnika, którego chcesz edytować:");
				dane = scanner.nextLine();
				
				ui.edytuj(zawodnicy.get(Integer.valueOf(dane)-1));
				//System.out.println(zawodnicy.get(Integer.valueOf(dane)-1).toString());
				
				file = new FileOperations(folder,zawodnicy.get(Integer.valueOf(dane)-1).getName(),zawodnicy.get(Integer.valueOf(dane)-1).getLastName());
				System.out.println(zawodnicy.get(Integer.valueOf(dane)-1).getInfoToFile());
		        file.zapis(zawodnicy.get(Integer.valueOf(dane)-1).getInfoToFile());
				
				break;
			case "3":
				for(Zawodnik z1: zawodnicy) {
					System.out.println((zawodnicy.indexOf(z1) + 1)+ " " + z1.getPlayerInfo());
				}
				break;
			case "END":
				System.exit(0);
			default:
				System.out.println("Niepoprawna komenda.");
			}
		}
	}
	
}
