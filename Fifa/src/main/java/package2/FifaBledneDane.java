package package2;

public class FifaBledneDane extends Exception {

	private static final long serialVersionUID = 7254876143245802167L;
	
	public FifaBledneDane() {
		super("Niepoprawny format danych.");
	}

}
